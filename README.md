# chalice-http-toolkit

[![Documentation Status](https://readthedocs.org/projects/chalice-http-toolkit/badge/?version=latest)](https://chalice-http-toolkit.readthedocs.io/en/latest/?badge=latest)

chalice-http-toolkit enables a Flask like website building experience using [Chalice](https://aws.github.io/chalice/), AWS Lambda, API Gateway & CloudFront. It does this by bolting on Jinja2 templates, and CloudFront cache management layer.

For full setup guide see the [documentation](https://chalice-http-toolkit.readthedocs.io/en/latest/).

## License

chalice-http-toolkit is licensed under the [GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/#) license.