import os
import imghdr
import traceback
from datetime import datetime
from chalice import Chalice, Response
from chalice_http_toolkit.content import ContentManager
from chalice_http_toolkit.cloudfront import CloudFront
from chalice_http_toolkit.multipart import ex_multipart

app = Chalice(app_name='example1')
app.api.binary_types.append('multipart/form-data')
app.api.binary_types.append('image/*')
app_dir = os.path.dirname(os.path.realpath(__file__))
upload_dir = os.path.join(app_dir, '../uploads')
chalicelib_dir = os.path.join(app_dir, 'chalicelib')
cm = ContentManager(os.path.join(chalicelib_dir, 'templates'),
                                 os.path.join(chalicelib_dir, 'static'), app=app)
cm.set_static_handler_prefix('/static')
cf = CloudFront(cm, app)

def render_index():
    return cf.template('index.html', cache_control='public, must-revalidate, max-age=604800')

def render_404():
    return cm.html(cm.render_template('404.html'), status_code=404)

@app.route('/static/{filename}')
def static_route(filename):
    r = cf.static(filename, cache_control='public, must-revalidate, max-age=604800')
    return r

@app.route('/{proxy+}')
def page_not_found():
    try:
        return render_404()
    except Exception as e:
        traceback.print_exc()

@app.route('/')
def index():
    try:
        return render_index()
    except Exception as e:
        traceback.print_exc()
    return render_404()

@app.route('/about')
def about():
    try:
        return cf.template('about.html', cache_control='public, must-revalidate, max-age=604800')
    except Exception as e:
        traceback.print_exc()
    return render_404()


@app.route('/image/{name}')
def image(name, content_types=['image/*']):
    try:
        def get_image():
            os.makedirs(upload_dir, exist_ok=True)
            # Would normally retrieve this from a db, for demo purposes load from file system
            with open(os.path.join(upload_dir, name), 'rb') as f:
                return f.read()
        def get_mod_date():
            os.makedirs(upload_dir, exist_ok=True)
            # Would normally retrieve this from a db, for demo purposes load from file system
            return datetime.fromtimestamp(os.path.getmtime(os.path.join(upload_dir, name)))
        accepts = app.current_request.headers.get('accept', '')
        get_etag = lambda: get_mod_date().strftime('%Y-%m-%d %H:%M:%S %Z') + accepts
        get_asset = lambda: cm.convert_by_accepts(get_image(), accepts)
        r = cf.asset(get_asset, get_etag, cache_control='public, max-age=604800, immutable')
        return r
    except Exception as e:
        traceback.print_exc()
    return render_404()

@app.route('/addimage', methods=['POST'], content_types=['multipart/form-data'])
def addimage():
    files = ex_multipart(app)

    fdata = files['file'].get('data')
    fname = files['file'].get('filename')
    ftype = imghdr.what(None, h=fdata)
    if ftype not in ['gif', 'png', 'jpeg', 'webp', 'bmp']:
        return render_404()

    os.makedirs(upload_dir, exist_ok=True)
    # Would normally save this to a db, for demo purposes save to file system
    with open(os.path.join(upload_dir, fname), 'wb') as f:
        f.write(fdata)
    return cm.html(cm.render_template('success.html', name=fname), status_code=200)
