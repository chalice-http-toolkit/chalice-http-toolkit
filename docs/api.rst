API Reference
==============================

Jinja2 API
-------------------
`static(<filename>)` is a function which resolves static contents path. A Chalice route should be set
which matches `ContentManager.set_static_handler_prefix()`'s value.

Python API
-------------------

chalice\_http\_toolkit.cloudfront module
**********************

.. automodule:: chalice_http_toolkit.cloudfront
   :members:
   :undoc-members:
   :show-inheritance:

chalice\_http\_toolkit.content module
**********************

.. automodule:: chalice_http_toolkit.content
   :members:
   :undoc-members:
   :show-inheritance:

chalice\_http\_toolkit.multipart module
**********************

.. automodule:: chalice_http_toolkit.multipart
   :members:
   :undoc-members:
   :show-inheritance:

chalice\_http\_toolkit.response\_with\_binary module
**********************

.. automodule:: chalice_http_toolkit.response_with_binary
   :members:
   :undoc-members:
   :show-inheritance:


chalice\_http\_toolkit.tasking module
**********************

.. automodule:: chalice_http_toolkit.tasking
   :members:
   :undoc-members:
   :show-inheritance: